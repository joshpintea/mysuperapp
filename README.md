# MySuperApp

### Setup

Download and install python 3.7

    sudo apt install python3.7
   
Install and create a virtual environment

    sudo apt-get install python3-pip
    sudo pip3 install virtualenv
    
    virtualenv -p /usr/bin/python3.7 venv
   
Install project dependencies
    
    source venv/bin/activate
    pip install -r requirments.txt

Install

    sudo apt-get install redis-server

Set environment variables
    
    DJANGO_SECRET
    ALLOWED_HOSTS # separated by semicolon
    SHOPIFY_APP_API_KEY
    SHOPIFY_APP_API_SECRET
    TWILIO_ACCOUNT_SID
    TWILIO_AUTH_TOKEN
    TWILIO_ACCOUNT_PHONE_NUMBER
    
### Make and apply migrations

    python manage.py makemigrations
    python manage.py migrate


## Start the development server

    python manage.py runserver


