from django.conf.urls import url
from django.urls import path
from shopify_webhook.views import WebhookView
from . import views as v

urlpatterns = [
    path('', v.home, name='home'),
    path('uninstall_app_web_hook/', v.app_uninstalled, name='webhooks'),
]