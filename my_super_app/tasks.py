from celery import shared_task
from django.urls import reverse

from .models import AuthAppShopUser
from twilio.rest import Client
from django.conf import settings
from celery import Celery

from . import faker

import shopify


celery = Celery(__name__)
celery.config_from_object(__name__)


client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_ACCOUNT_TOKEN)


@shared_task
def send_message(myshopify_domain):
    message_body = "Hy {FirstName}, it's great to see {StoreName} installed with out app. {CustomerCount} will love it"

    print("Start send_message process for %s" % myshopify_domain)

    try:
        shop_user = AuthAppShopUser.objects.get(myshopify_domain=myshopify_domain)
    except:
        print("Shop `%s` is not installed" % myshopify_domain)
        return

    with shop_user.session:
        print("Retrieve shop and customer count")
        shop, customer_count = shopify.Shop.current(), shopify.Customer.count()
        message_body = message_body.format(CustomerCount=customer_count, FirstName=shop.shop_owner.split(' ')[0], StoreName=shop.name)

        phone_number = shop.phone if shop.phone else faker.random_phone_number()
        try:
            print("Sending %s to %s." % (message_body, phone_number,))
            client.messages.create(to=phone_number,
                                   body=message_body,
                                   from_=settings.TWILIO_ACCOUNT_PHONE_NUMBER)
            print("Message successfully sent")
        except:
            print("Sending the sms failed")


@shared_task
def subscribe_to_uninstalled_app_hook(domain, myshopify_domain):
    """
    Subscribe to app/uninstalled hook for the given 'myshopify_domain' shop

    :argument domain The domain of the current site
    :argument myshopify_domain The domain of the installed shop
    """
    try:
        shop_user = AuthAppShopUser.objects.get(myshopify_domain=myshopify_domain)
    except:
        print("Shop `%s` is not installed" % myshopify_domain)
        return

    with shop_user.session:
        hook = shopify.Webhook()
        hook._headers = {
            "X-Shopify-Access-Token": shop_user.token,
            "Content-Type": "application/json"
        }
        hook.topic = 'app/uninstalled'
        hook.address = 'https://{domain}{uri}'.format(domain=domain, uri=reverse('webhooks'))
        registered = hook.save()
        print(registered)

