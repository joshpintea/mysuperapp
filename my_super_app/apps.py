from django.apps import AppConfig


class MySuperAppConfig(AppConfig):
    name = 'my_super_app'

    def ready(self):
        import my_super_app.tasks