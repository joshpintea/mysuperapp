from django.conf import settings
from django.contrib.auth.backends import RemoteUserBackend
from django.contrib.sites.shortcuts import get_current_site

from .models import AuthAppShopUser

from django.utils import timezone
from datetime import timedelta

from .tasks import subscribe_to_uninstalled_app_hook
from .tasks import send_message

import logging

logger = logging.getLogger(__file__)


class ShopUserBackend(RemoteUserBackend):
    def authenticate(self, request=None, myshopify_domain=None, token=None, **kwargs):
        if not myshopify_domain or not token or not request:
            return

        exists = AuthAppShopUser.objects.filter(myshopify_domain=myshopify_domain).count() > 0

        user = super(ShopUserBackend, self).authenticate(request=request, remote_user=myshopify_domain)

        if not user:
            return

        user.token = token
        user.save()

        if not exists:
            subscribe_to_uninstalled_app_hook.delay(domain=str(get_current_site(request)), myshopify_domain=myshopify_domain)
            send_message.apply_async([myshopify_domain],
                                     eta=timezone.now() + timedelta(seconds=settings.DELAYED_NUMBER_OF_SECONDS))

        return user