from django.http import HttpResponse
from django.shortcuts import render
from shopify_auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from shopify_webhook.decorators import webhook

from my_super_app.models import AuthAppShopUser


@login_required
def home(request):
    return render(request, 'my_super_app/home.html', {'title': 'Home'})


@csrf_exempt
@webhook
def app_uninstalled(request):
    if request.method == 'POST':
        AuthAppShopUser.objects.filter(myshopify_domain=request.webhook_data['domain']).delete()

    return HttpResponse("Ok")