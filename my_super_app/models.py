import json

import requests

from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.urls import reverse
from shopify_auth.models import AbstractShopUser

# Create your models here.


class AuthAppShopUser(AbstractShopUser):
    pass