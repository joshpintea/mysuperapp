import random


def random_phone_number():
    return '+' + "".join([str(random.randint(0, 9)) for _ in range(11)])