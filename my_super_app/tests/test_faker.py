from django.test import TestCase
from my_super_app.faker import random_phone_number


import re


class FakeTestCase(TestCase):

    def test_generate_random_number(self):
        regex = re.compile(r'\+\d{11}')

        number = random_phone_number()
        result = regex.findall(number)

        self.assertEqual(1, len(result))
        self.assertEqual(number, result[0])