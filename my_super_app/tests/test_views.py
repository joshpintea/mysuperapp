import json
import signal

from django.test import TestCase
from django.test import Client

from django.conf import settings
from django.urls import reverse
from shopify_webhook.helpers import get_hmac



from my_super_app.models import AuthAppShopUser


class HomeViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def tearDown(self):
        self.client = None

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('home'))
        self.assertRedirects(response, '/login/?next=/')


class UninstalledWebHookTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        # create a shop
        shop = AuthAppShopUser()
        shop.myshopify_domain = "test.myshopify.com"
        shop.token = "000000"

        shop.save()

    def tearDown(self):
        settings.SHOPIFY_APP_DEV_MODE = False
        self.client = None

    def _post_shopify_webhook(self, url, topic=None, domain=None, data=None, headers=None, send_hmac=True):
        """
        Simulate a webhook being sent to the application's webhook endpoint with the provided parameters.
        """
        # Set defaults.
        domain = 'test.myshopify.com' if domain is None else domain
        data = {} if data is None else data
        headers = {} if headers is None else headers

        # Dump data as a JSON string.
        data = json.dumps(data)

        # Add required headers.
        headers['HTTP_X_SHOPIFY_TEST'] = 'true'
        headers['HTTP_X_SHOPIFY_SHOP_DOMAIN'] = domain

        # Add optional headers.
        if topic:
            headers['HTTP_X_SHOPIFY_TOPIC'] = topic
        if send_hmac:
            headers['HTTP_X_SHOPIFY_HMAC_SHA256'] = str(
                get_hmac(data.encode("latin-1"), settings.SHOPIFY_APP_API_SECRET))

        return self.client.post(url, data=data, content_type='application/json', **headers)

    def test_method_not_allowed(self):
        response = self.client.get(reverse('webhooks'))
        self.assertEqual(405, response.status_code)

    def test_shop_user_is_deleted(self):
        """
        Post a request to the webhook uri and check if the userShop if deleted from database
        """
        settings.SHOPIFY_APP_DEV_MODE = True

        self.assertEqual(AuthAppShopUser.objects.all().count(), 1)
        response = self._post_shopify_webhook(reverse('webhooks'),
                                              topic="app/uninstalled",
                                              data={'domain': 'test.myshopify.com'})

        self.assertEqual(200, response.status_code)
        self.assertEqual(AuthAppShopUser.objects.all().count(), 0)

    def test_shop_is_not_deleted(self):
        settings.SHOPIFY_APP_DEV_MODE = True

        self.assertEqual(AuthAppShopUser.objects.all().count(), 1)
        response = self._post_shopify_webhook(reverse('webhooks'),
                                              topic="app/uninstalled",
                                              data={'domain': 'not.installed.com'})

        self.assertEqual(200, response.status_code)
        self.assertEqual(1, AuthAppShopUser.objects.all().count())